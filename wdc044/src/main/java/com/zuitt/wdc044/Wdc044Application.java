package com.zuitt.wdc044;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class Wdc044Application {

	public static void main(String[] args) {
		SpringApplication.run(Wdc044Application.class, args);
	}

//	run app ./mvnw spring-boot:run

	@GetMapping("/hello")
	public String hello(@RequestParam(value = "name", defaultValue = "World") String name) {
		return String.format("Hello %s", name);
	}

	@GetMapping("/greetings")
	public String greetings(@RequestParam(value = "greet", defaultValue = "World") String greet) {
		return String.format("Good evening %s! Welcome to Batch 293!", greet);
	}

//	ACTIVITY
	@GetMapping("/hi")
	public String hi(@RequestParam(value = "user", defaultValue = "User") String user) {
		return String.format("Hi %s!", user);
	}

	@GetMapping("/nameAge")
	public String nameAge(
			@RequestParam(value = "user", defaultValue = "User") String user,
			@RequestParam(value = "age", defaultValue = "0") int age) {
		return String.format("Rainy Eve %s! You are %d years old.", user, age);
	}

}
