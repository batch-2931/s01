package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.models.User;

import java.util.Optional;
import java.util.Set;

public interface UserService {
    void createUser(User user);

    Optional<User> findByUsername(String username);

    //Mini-Activity
    //Modify the UserController and create a getUser() function that retrieves a specific user using the token, when a request is received at the "/userProfile" endpoint.
    //In returning the password, it should be an empty string.
    Object getUser(String stringToken);

//    ACTIVITY
    Set<Post> getPosts(String stringToken);

}
