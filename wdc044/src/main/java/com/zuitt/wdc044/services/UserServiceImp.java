package com.zuitt.wdc044.services;

import com.zuitt.wdc044.config.JwtToken;
import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.models.User;
import com.zuitt.wdc044.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;

@Service
public class UserServiceImp implements UserService{
    @Autowired
    private UserRepository userRepository;

    @Autowired
    JwtToken jwtToken;

    public void createUser(User user) {
        userRepository.save(user);
    }
    public Optional<User> findByUsername(String username) {
        return Optional.ofNullable(userRepository.findByUsername(username));
    }

    //Mini-Activity
    //Modify the UserController and create a getUser() function that retrieves a specific user using the token, when a request is received at the "/userProfile" endpoint.
    //In returning the password, it should be an empty string.
    public Object getUser(String stringToken){
        User user = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        user.setPassword("");
        return user;
    }

//    ACTIVITY
    public Set<Post> getPosts(String stringToken) {
        Optional<User> userDetails = Optional.ofNullable(userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken)));
        if (userDetails.isPresent()) {
            User userPosts = userDetails.get();
            userPosts.setPassword("");
            return userPosts.getPosts();
        }
        return null;
    }
}
